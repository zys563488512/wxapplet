#### iOS SDK接入流程
##### 一、新建工程,并引入framework和WeChatSDK
<image src="/Users/yifutong/Desktop/iOSSDK接入流程/images/img1.jpg" />
<image src="/Users/yifutong/Desktop/iOSSDK接入流程/images/img3.jpg" />

##### 二、开启定位授权，在info.plist
<image src="/Users/yifutong/Desktop/iOSSDK接入流程/images/img2.jpg" />

##### 三、获取定位信息，Demo中使用原生定位
引入头文件

```
#import <CoreLocation/CoreLocation.h>
```

添加代理

```
#@interface ViewController () <CLLocationManagerDelegate>
```

定义CLLocationManager对象和经纬度

```
@property (nonatomic,strong) CLLocationManager *locationManager;
@property (nonatomic,strong) NSString * latitude;
@property (nonatomic,strong) NSString * longtitude;
```

初始化

```
- (void)viewDidLoad {
    [super viewDidLoad];
    if (!_locationManager) {
        _locationManager = [[CLLocationManager alloc] init];
    }
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 8.0) {
        //提示用户开启定位
        [_locationManager requestAlwaysAuthorization];
    }
    _locationManager.delegate = self;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    _locationManager.distanceFilter = 5.0;
    
    //开启定位
    
    [_locationManager startUpdatingLocation];
}
```

实现协议

```
#pragma mark - CLLocationManagerDelegate
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    CLLocation *newLocation = [locations lastObject];
    //使用当前坐标
    self.latitude = [NSString stringWithFormat:@"%f",newLocation.coordinate.latitude];
    self.longtitude = [NSString stringWithFormat:@"%f",newLocation.coordinate.longitude];
    //关闭定位
    [manager stopUpdatingLocation];
}
```
##### 四、拿到定位信息进行加密
引入头文件

```
#import <wxPayFramework/WXAppletAPI.h>
#import <wxPayFramework/WXPayGetAPI.h>

WXPayGetAPI * api = [[WXPayGetAPI alloc] init];
NSString * baseString = [api getWXAPPInfo:self.longtitude latitude:self.latitude];
```

#### 五、调起微信小程序
WXAppletDelegate

```
//调起微信小程序
- (void)getWXApplet:(OrderModel *)orderModel {
    WXAppletAPI * applet = [[WXAppletAPI alloc] init];
    BOOL isSuccess = [applet getWXApplet:orderModel];
    if (isSuccess) {
        NSLog(@"成功");
    }else {
        NSLog(@"失败");
    }
}
- (void)onWXReq:(BaseReq *)req {
    
}
- (void)onWXResp:(BaseResp *)resp {
    
}
```