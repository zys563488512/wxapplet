//
//  WXPayAPI.h
//  Demo
//
//  Created by yifutong on 2018/12/17.
//  Copyright © 2018年 yifutong. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol WXPayGetAPIDelegate <NSObject>
@required //强制方法列表
- (void)getResponseData:(NSDictionary *) dict;
@end

@interface WXPayAPI : NSObject
@property (nonatomic, strong) NSMutableData *responseData;
@property(nonatomic,weak) id<WXPayGetAPIDelegate> delegate;
-(void)delegateTest:(NSString *)baseString;
@end

NS_ASSUME_NONNULL_END
