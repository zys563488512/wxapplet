//
//  ViewController.m
//  Demo
//
//  Created by yifutong on 2018/12/17.
//  Copyright © 2018年 yifutong. All rights reserved.
//

#import "ViewController.h"
#import <CoreLocation/CoreLocation.h>
#import <wxPayFramework/WXAppletAPI.h>
#import <wxPayFramework/WXPayGetAPI.h>
#import <wxPayFramework/OrderModel.h>
#import "WXPayAPI.h"
@interface ViewController ()<CLLocationManagerDelegate,WXPayGetAPIDelegate,WXAppletDelegate>
@property (nonatomic,strong) CLLocationManager *locationManager;
@property (nonatomic,strong) NSString * latitude;
@property (nonatomic,strong) NSString * longtitude;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(100, 100, 200, 40)];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button setBackgroundColor:[UIColor blueColor]];
    [button setTitle:@"唤起小程序" forState:UIControlStateNormal];
    [button addTarget:self action:@selector(getLocationPwd) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
    
    
    if (!_locationManager) {
        _locationManager = [[CLLocationManager alloc] init];
    }
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 8.0) {
        //提示用户开启定位
        [_locationManager requestAlwaysAuthorization];
    }
    _locationManager.delegate = self;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    _locationManager.distanceFilter = 5.0;
    
    //开启定位
    
    [_locationManager startUpdatingLocation];
}
//加密定位信息
- (void)getLocationPwd {
    NSLog(@"getLocationPwd");
    WXPayGetAPI * api = [[WXPayGetAPI alloc] init];
    NSString * baseString = [api getWXAPPInfo:self.longtitude latitude:self.latitude];
    WXPayAPI * pay = [[WXPayAPI alloc] init];
    pay.delegate = self;
    [pay delegateTest:baseString];
}
- (void)getResponseData:(NSDictionary *)dict {
    NSLog(@"------ %@",dict);
    OrderModel * orderModel = [[OrderModel alloc] init];
    orderModel.userName = @"拉起的小程序的username";
    orderModel.path = @"拉起小程序页面的路径，不填默认拉起小程序首页";
    orderModel.miniProgramType = WXMiniProgramTypeTest; //拉起小程序的类型
    orderModel.extMsg = @""; //json格式
    [self getWXApplet:orderModel];
}
//调起微信小程序
- (void)getWXApplet:(OrderModel *)orderModel {
    WXAppletAPI * applet = [[WXAppletAPI alloc] init];
    BOOL isSuccess = [applet getWXApplet:orderModel];
    if (isSuccess) {
        NSLog(@"成功");
    }else {
        NSLog(@"失败");
    }
}
- (void)onWXReq:(BaseReq *)req {
    
}
- (void)onWXResp:(BaseResp *)resp {
    
}
//实现协议

#pragma mark - CLLocationManagerDelegate
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    CLLocation *newLocation = [locations lastObject];
    //使用当前坐标
    self.latitude = [NSString stringWithFormat:@"%f",newLocation.coordinate.latitude];
    self.longtitude = [NSString stringWithFormat:@"%f",newLocation.coordinate.longitude];
    //关闭定位
    [manager stopUpdatingLocation];
    
}

@end
