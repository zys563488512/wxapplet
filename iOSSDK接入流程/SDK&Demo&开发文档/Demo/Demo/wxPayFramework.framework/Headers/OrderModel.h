//
//  OrderModel.h
//  WXPayDemo
//
//  Created by yifutong on 2018/12/14.
//  Copyright © 2018年 yifutong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WXApi.h"
@interface OrderModel : NSObject
@property (nonatomic, strong) NSString *userName;   //拉起的小程序的username
@property (nonatomic, strong) NSString *path;       //拉起小程序页面的路径，不填默认拉起小程序首页
@property (nonatomic, assign) WXMiniProgramType miniProgramType; //拉起小程序的类型

@property (nonatomic, strong) NSString *extMsg; //json格式
@end
