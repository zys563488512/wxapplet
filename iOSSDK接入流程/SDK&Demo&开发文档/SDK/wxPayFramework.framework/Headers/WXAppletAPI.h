//
//  WXAppletAPI.h
//  wxPayFramework
//
//  Created by yifutong on 2018/12/17.
//  Copyright © 2018年 yifutong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OrderModel.h"
#import "WXApi.h"
@protocol WXAppletDelegate <NSObject>
@required
/*! @brief 收到一个来自微信的请求，第三方应用程序处理完后调用sendResp向微信发送结果
 *
 * 收到一个来自微信的请求，异步处理完成后必须调用sendResp发送处理结果给微信。
 * 可能收到的请求有GetMessageFromWXReq、ShowMessageFromWXReq等。
 * @param req 具体请求内容，是自动释放的
 */
- (void)onWXReq:(BaseReq *)req;
- (void)onWXResp:(BaseResp *)resp;
@end
@interface WXAppletAPI : NSObject <WXApiDelegate>
@property (nonatomic,weak) id<WXAppletDelegate> delegate;
- (BOOL) getWXApplet:(OrderModel *) orderModel;
@end

