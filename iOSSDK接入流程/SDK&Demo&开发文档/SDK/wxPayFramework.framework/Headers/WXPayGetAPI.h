//
//  WXPayGetAPI.h
//  WXPayDemo
//
//  Created by yifutong on 2018/12/14.
//  Copyright © 2018年 yifutong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WXPayGetAPI : NSObject
//传入经纬度
- (NSString *)getWXAPPInfo:(NSString *)longtitude latitude:(NSString *)latitude;

@end
